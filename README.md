# gst-videolooper


## Overview
This tool is designed to play a video file to a loopback device and monitor display changes on a Raspberry Pi. It uses GStreamer for video processing and integrates with Wayland for display management. It also supports mouse automation and dynamic handling of display configuration changes.

## Features

* Video Playback: Play video files to a specified loopback device.
* Display Monitoring: Continuously monitor and react to display configuration changes.
* Codec Support: Automatically selects the appropriate decoder based on the Raspberry Pi model and video codec.
* Mouse Automation: Moves the mouse to the center and performs a double-click on active displays.
* Error Handling: Robust error handling for GStreamer pipeline and external commands.

## Prerequisites

* Python 3.x
* GStreamer 1.0 with Python bindings
* Raspberry Pi (tested on Raspberry Pi 4)
* ffprobe from the FFmpeg suite for video analysis
* ydotool for mouse automation
* wlr-randr for display configuration management
* pipewire as audio backend (pulsemixer for command line volume adjustement)

## Installation

### Install GStreamer and Python bindings:

```
sudo apt-get install python3-gst-1.0 gstreamer1.0-pipewire

```

### Install FFmpeg and ydotool:

```
sudo apt-get install ffmpeg ydotool
```

### Clone this repository:

```
url
```

## Usage

### Run the script with the path to the video file:

```
python3 [script_name.py] /path/to/video.mp4
```

Optionally, specify a loopback device using the --loopback-device argument.

## Functionality Overview

* on_message: Handles GStreamer bus messages for EOS and ERROR.
* build_decoder_pipeline: Selects the appropriate GStreamer decoder based on the video codec and Raspberry Pi model.
* get_video_properties_ffprobe: Extracts video codec and frame rate using ffprobe.
* get_raspberry_pi_model: Detects the model of the Raspberry Pi.
* create_gstreamer_pipeline: Creates and starts a GStreamer pipeline for video playback.
* move_mouse_to_center_and_double_click: Automates mouse movement and clicking.
* get_display_configuration: Retrieves current display configuration using wlr-randr.
* parse_display_configuration: Parses the display configuration data.
* print_monitor_info: Prints information about connected monitors.
* start_display_monitoring: Starts a thread for monitoring display changes.
* monitor_display_changes: Monitors and reacts to changes in display configuration.
* main: Entry point of the script, handling command-line arguments and setting up the main GStreamer pipeline.