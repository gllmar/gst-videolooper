#!/bin/bash

# Check if a video path is provided
if [ "$#" -lt 1 ]; then
    echo "Usage: $0 /path/to/your/video.mp4 [loopback_device]"
    exit 1
fi

VIDEO_PATH="$1"
LOOPBACK_DEVICE="${2:-/dev/video61}" # Default to /dev/video61 if no device is specified

# Set XDG_RUNTIME_DIR for PipeWire
export XDG_RUNTIME_DIR="/run/user/1000"

# GStreamer pipeline
PIPELINE="filesrc location=\"$VIDEO_PATH\" ! decodebin name=demux \
          demux. ! queue ! videoconvert ! tee name=t \
          t. ! queue ! autovideosink \
          t. ! queue ! v4l2sink device=$LOOPBACK_DEVICE \
          demux. ! queue ! audioconvert ! autoaudiosink"

# Execute the pipeline
gst-launch-1.0 -e $PIPELINE

