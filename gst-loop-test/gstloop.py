import gi
import sys
import os

gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

# Initialize GStreamer
Gst.init(None)

class VideoPlayer:
    def __init__(self, video_path):
        self.video_path = video_path
        self.loop = GLib.MainLoop()
        self.build_pipeline()

    def build_pipeline(self):
        # GStreamer pipeline with audio support
        pipeline_desc = (
            "filesrc location=\"" + self.video_path + "\" ! decodebin name=demux "
            "demux. ! queue ! videoconvert ! autovideosink "
            "demux. ! queue ! audioconvert ! autoaudiosink"
        )

        self.pipeline = Gst.parse_launch(pipeline_desc)
        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect("message::eos", self.on_eos)
        self.bus.connect("message::error", self.on_error)

    def on_eos(self, bus, msg):
        print("End-Of-Stream reached. Looping.")
        self.pipeline.seek_simple(
            Gst.Format.TIME,
            Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
            0
        )

    def on_error(self, bus, msg):
        err, debug_info = msg.parse_error()
        print(f"Error received from element {msg.src.get_name()}: {err.message}")
        print(f"Debugging information: {debug_info if debug_info else 'none'}")
        self.loop.quit()

    def run(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        try:
            self.loop.run()
        except KeyboardInterrupt:
            pass
        self.pipeline.set_state(Gst.State.NULL)

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: python script.py /path/to/your/video.mp4")
        sys.exit(1)

    # Set XDG_RUNTIME_DIR for PipeWire
    os.environ["XDG_RUNTIME_DIR"] = "/run/user/1000"
    video_path = sys.argv[1]
    player = VideoPlayer(video_path)
    player.run()
