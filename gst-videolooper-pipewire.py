import gi
import sys
import argparse
import subprocess
import os
import time
import threading
import re
import json

global_args = None
global_player = None
global videolooper_node_id

gi.require_version('Gst', '1.0')
from gi.repository import Gst, GLib

os.environ['WAYLAND_DISPLAY'] = 'wayland-1'

# Initialize GStreamer
Gst.init(None)

# Global dictionary to store active GStreamer pipelines for each display
pipelines = {}

def on_message(bus, message, loop, player):
    if message.type == Gst.MessageType.EOS:
        # End of stream, seek to start
        player.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT, 0)
    elif message.type == Gst.MessageType.ERROR:
        err, debug = message.parse_error()
        print(f"Error: {err}, {debug}", file=sys.stderr)

        # Check if the error is related to audio disconnection
        if 'gst-resource-error-quark' in str(err) and 'Disconnected' in str(err):
            print("Audio device disconnected. Attempting to handle gracefully.")
            # Pause or stop the audio pipeline
            # player.set_state(Gst.State.PAUSED)
            # Optionally, implement a retry mechanism or wait for reconnection
            handle_audio_disconnection(player)
        else:
            # For other errors, quit the loop
            loop.quit()
    return True


def handle_audio_disconnection(player):
    print("Handling audio disconnection. Restarting video decoding.")
    stop_all_pipelines()

    # Restart video decoding
    # Assuming args is accessible here, or pass it as an argument
    create_video_decoding_pipeline(global_args)
    connect_audio_ports("python")  # Replace "python" with your desired output source

    # Assuming you have a function to reset the screen position
    reset_screen_position()


def reset_screen_position():
    # Mimic monitor change behavior
    time.sleep(0.5)
    current_info = get_display_configuration()
    current_monitors = parse_display_configuration(current_info)
    move_mouse_to_center_and_double_click(current_monitors)
   



def build_decoder_pipeline(codec, pi_model):
    if codec == 'h264':
        if 'Raspberry Pi 4' in pi_model:
            return 'h264parse ! v4l2h264dec'
        else:
            return 'h264parse ! avdec_h264'
    elif codec == 'hevc' or codec == 'h265':
        return 'h265parse ! avdec_h265'
    elif codec == 'vp8':
        return 'vp8dec'
    elif codec == 'vp9':
        return 'vp9dec'
    elif codec == 'av1':
        return 'av1dec'
    else:
        return 'decodebin'

def get_video_properties_ffprobe(filepath):
    command = [
        'ffprobe',
        '-v', 'error',
        '-select_streams', 'v:0',
        '-show_entries', 'stream=codec_name,r_frame_rate',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        filepath
    ]
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if result.returncode == 0:
        codec, frame_rate = result.stdout.strip().split('\n')
        return codec, frame_rate
    else:
        print(f"Failed to get video properties: {result.stderr}", file=sys.stderr)
        sys.exit(1)

def get_audio_properties_ffprobe(filepath):
    command = [
        'ffprobe',
        '-v', 'error',
        '-select_streams', 'a:0',
        '-show_entries', 'stream=codec_name',
        '-of', 'default=noprint_wrappers=1:nokey=1',
        filepath
    ]
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    if result.returncode == 0:
        audio_codec = result.stdout.strip()
        return audio_codec
    else:
        print(f"Failed to get audio properties: {result.stderr}", file=sys.stderr)
        sys.exit(1)

def build_audio_decoder_pipeline(codec):
    if codec == 'aac':
        return 'aacparse ! avdec_aac'
    elif codec == 'mp3':
        return 'mpegaudioparse ! avdec_mp3'
    # Add more codecs as needed
    else:
        return 'decodebin'


def get_raspberry_pi_model():
    try:
        with open('/proc/device-tree/model', 'r') as model_file:
            model = model_file.read()
        return model
    except Exception as e:
        print(f"Error reading Raspberry Pi model: {e}", file=sys.stderr)
        return None   


def move_mouse_to_center_and_double_click(info):
    global pipelines

    # Disable existing GStreamer pipelines if they're running
    for display, pipeline in pipelines.items():
        pipeline.set_state(Gst.State.NULL)
    pipelines = {}

    for monitor_name, monitor_info in info.items():
        enabled = monitor_info.get('Enabled', 'No')
        if enabled.lower() == 'yes':
            position = monitor_info.get('Position', 'N/A')
            if position != 'N/A':
                pos_x, pos_y = map(int, position.split(','))
                resolution = monitor_info.get('Resolution', 'N/A')
                if resolution != 'N/A':
                    width, height = map(int, resolution.split(' ')[0].split('x'))
                    center_x = pos_x + width // 2
                    center_y = pos_y + height // 2
                    
                    print(f"Double-clicking at ({center_x}, {center_y})")  # Print the click location
                    # Use ydotool to move the mouse to the center and double-click
                    subprocess.run(['sudo', 'ydotool', 'mousemove', '--absolute', f'-x {center_x/2}', f'-y {center_y/2}'])
                    subprocess.run(['sudo', 'ydotool', 'click', '--repeat', '1', '--next-delay', '25', '0xC0'])

                    
                    # Add a small delay after the double-click
                    time.sleep(.1)  # Adjust the delay as needed

                    # Create and launch a GStreamer pipeline for this display
                    pipelines[monitor_name] = create_gstreamer_display_pipeline()
                    time.sleep(.1)  # Adjust the delay as needed
    subprocess.run(['sudo', 'ydotool', 'mousemove', '--absolute', f'-x 32000', f'-y 32000'])  

def get_display_configuration():
    result = subprocess.run(['wlr-randr'], stdout=subprocess.PIPE, text=True)
    return result.stdout.strip()

def parse_display_configuration(config):
    monitors = {}
    lines = config.split('\n')
    current_monitor = None

    for line in lines:
        if line.strip().startswith('DSI') or line.strip().startswith('HDMI'):
            current_monitor = line.strip().split(' ')[0]
            monitors[current_monitor] = {}
        elif current_monitor:
            parts = line.strip().split(':')
            if len(parts) == 2:
                key = parts[0].strip()
                value = parts[1].strip()
                monitors[current_monitor][key] = value
            elif "current" in line:
                monitors[current_monitor]['Resolution'] = line.strip()

    return monitors

def print_monitor_info(monitors):
    for monitor_name, info in monitors.items():
        print(f"{monitor_name} Enabled: {info.get('Enabled', 'No')}")
        resolution = info.get('Resolution', 'N/A')
        print(f"Resolution: {resolution}")
        print(f"Position: {info.get('Position', 'N/A')}")

def start_display_monitoring():
    initial_info = get_display_configuration()
    print("Initial Monitor State:")
    initial_monitors = parse_display_configuration(initial_info)
    print_monitor_info(initial_monitors)
    move_mouse_to_center_and_double_click(initial_monitors)
    

def create_gstreamer_display_pipeline():
    # Using pipewiresrc to fetch the video from the specified PipeWire node
    pipeline_str = f'pipewiresrc path={videolooper_node_id} ! videoconvert ! waylandsink fullscreen=true'
    pipeline = Gst.parse_launch(pipeline_str)
    pipeline.set_state(Gst.State.PLAYING)
    return pipeline


def create_video_decoding_pipeline(args):
    global global_player
    global videolooper_node_id  # Declare the global variable
    if global_player is not None:
        global_player.set_state(Gst.State.NULL)
        global_player = None

    pi_model = get_raspberry_pi_model()
    if pi_model is None:
        print("Unable to determine Raspberry Pi model. Exiting.")
        sys.exit(1)

    codec, frame_rate = get_video_properties_ffprobe(args.filepath)
    print(f"Detected codec: {codec}")
    print(f"Raspberry Pi Model: {pi_model}")

    # Extract video and audio codec information
    video_codec, frame_rate = get_video_properties_ffprobe(args.filepath)
    audio_codec = get_audio_properties_ffprobe(args.filepath)

    # Build video and audio decoder pipelines
    video_decoder_pipeline = build_decoder_pipeline(video_codec, pi_model)
    audio_decoder_pipeline = build_audio_decoder_pipeline(audio_codec)

    # Define the pipeline with a custom PipeWire node name
    node_name = "gst-videolooper"  # Replace with your desired node name


# Define the pipeline string with a unique property
    pipeline_desc = f'filesrc location="{args.filepath}" ! qtdemux name=demux '
    pipeline_desc += f'demux.video_0 ! queue ! {video_decoder_pipeline} ! videoconvert ! videoscale '
    pipeline_desc += f'! video/x-raw,format=I420 ! pipewiresink stream-properties="props,media.class=Video/Source,media.role={node_name}" '
    pipeline_desc += f'demux.audio_0 ! queue ! {audio_decoder_pipeline} ! audioconvert ! audioresample ! autoaudiosink '

    try:
        player = Gst.parse_launch(pipeline_desc)
        player.set_state(Gst.State.PLAYING)
        global_player = player
        
        print("Pipeline restarted successfully.")
        bus = player.get_bus()
        bus.add_watch(0, on_message, GLib.MainLoop(), player)
    except Exception as e:
        print(f"Failed to restart pipeline: {e}")

        # Fetch the PipeWire node ID
    time.sleep(1)  # Give some time for the pipeline to start and register with PipeWire
    pipewire_objects = get_pipewire_objects()
    matching_objects = search_pipewire_objects(pipewire_objects, "gst-videolooper", "PipeWire:Interface:Node")

    if matching_objects:
        for obj in matching_objects:
            print(f"PipeWire Node ID: {obj['id']}, Name: {obj['info'].get('props', {}).get('node.name', 'N/A')}, Media Role: {obj['info'].get('props', {}).get('media.role', 'N/A')}")
            videolooper_node_id = obj['id']  # Set the global variable
    else:
        print("No matching PipeWire nodes found.")

    return player


def get_pipewire_objects():
    try:
        # Execute the 'pw-dump' command and parse JSON output
        result = subprocess.check_output(['pw-dump'], text=True)
        return json.loads(result)
    except subprocess.CalledProcessError as e:
        print("Failed to execute pw-dump:", e)
        return []

def search_pipewire_objects(objects, search_string, additional_filter=None):
    results = []

    for obj in objects:
        if obj.get('type') == additional_filter:
            props = obj.get('info', {}).get('props', {})
            for key, value in props.items():
                # Convert value to string before checking
                if search_string in str(value):
                    results.append({'id': obj['id'], 'type': obj['type'], 'info': obj['info']})
                    break  # Break the loop if match is found

    return results

# Signal handler for graceful shutdown
def stop_all_pipelines():
    for pipeline in list(pipelines.values()):
        pipeline.set_state(Gst.State.NULL)
    pipelines.clear()
    

def signal_handler(sig, frame):
    print("\nSignal received, stopping playback and exiting.")
    cleanup_and_exit()

def cleanup_and_exit():
    # Stop any GStreamer pipelines
    for pipeline in pipelines.values():
        pipeline.set_state(Gst.State.NULL)
    
    # Stop the main player if it's defined
    if global_player is not None:
        global_player.set_state(Gst.State.NULL)
    
    
    sys.exit(0)

def connect_audio_ports(output_source_name):
    time.sleep(.5)
    def run_command(command):
        result = subprocess.run(command, shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return result.stdout.strip()

    output_exists_FL = run_command(f"pw-link -o | grep '{output_source_name}:output_FL'")
    output_exists_FR = run_command(f"pw-link -o | grep '{output_source_name}:output_FR'")

    if not output_exists_FL or not output_exists_FR:
        print(f"Specified output source '{output_source_name}' does not exist or is incomplete.")
        return

    input_ports = run_command("pw-link -i | grep 'alsa_output'").split('\n')

    for line in input_ports:
        alsa_input = line.split()[-1]
        if ":playback_FL" in alsa_input:
            run_command(f"pw-link {output_source_name}:output_FL {alsa_input}")
            print(f"Connected {output_source_name}:output_FL to {alsa_input}")
        elif ":playback_FR" in alsa_input:
            run_command(f"pw-link {output_source_name}:output_FR {alsa_input}")
            print(f"Connected {output_source_name}:output_FR to {alsa_input}")

    print("All matching ALSA inputs have been connected.")


def get_first_video_file(directory_path):
    # Supported video file extensions
    video_extensions = ['.mp4', '.mkv', '.avi', '.mov', '.flv', '.wmv']
    video_files = [f for f in os.listdir(directory_path) if os.path.splitext(f)[1].lower() in video_extensions]
    video_files.sort(key=lambda x: x.lower())  # Sort the files in a case-insensitive manner

    # Print the sorted list of available video files
    print("Sorted list of available video files:")
    for video in video_files:
        print(video)

    return os.path.join(directory_path, video_files[0]) if video_files else None

def main():
    Gst.init(None)
    global subprocess_ref
    global global_args

    parser = argparse.ArgumentParser(description='Play a video file or the first video in a folder to a loopback device and monitor display changes.')
    parser.add_argument('filepath', help='The path to the video file or folder to play.')
    args = parser.parse_args()

    # Check if the filepath is a directory and get the first video file
    if os.path.isdir(args.filepath):
        first_video_file = get_first_video_file(args.filepath)
        if not first_video_file:
            print("No video files found in the directory.")
            sys.exit(1)
        args.filepath = first_video_file

    player = create_video_decoding_pipeline(args)
    connect_audio_ports("python")  # Replace "python" with your desired output source
    global_player = player
    global_args = args

    # Start display monitoring in a separate thread
    start_display_monitoring()

    try:
        GLib.MainLoop().run()
    except KeyboardInterrupt:
        print("\nStopping playback and exiting.")
        # Stop any GStreamer pipelines
        for pipeline in pipelines.values():
            pipeline.set_state(Gst.State.NULL)
        if player is not None:
            player.set_state(Gst.State.NULL)

        sys.exit(0)

if __name__ == '__main__':
    main()
