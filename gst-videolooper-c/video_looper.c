#include <gst/gst.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <stdio.h>

static GstElement *pipeline;

// Callback function for handling messages on the GStreamer bus
static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data) {
    GMainLoop *loop = (GMainLoop *)data;

    switch (GST_MESSAGE_TYPE(msg)) {
        case GST_MESSAGE_EOS:
            g_print("End of stream\n");
            // Seek to the beginning of the media to loop playback
            gst_element_seek_simple(pipeline, GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH | GST_SEEK_FLAG_KEY_UNIT, 0);
            break;
        case GST_MESSAGE_ERROR: {
            gchar *debug;
            GError *error;

            // Parse and print error message
            gst_message_parse_error(msg, &error, &debug);
            g_printerr("Error: %s\nDebug info: %s\n", error->message, debug ? debug : "none");
            g_free(debug);
            g_error_free(error);

            // Quit the main loop on error
            g_main_loop_quit(loop);
            break;
        }
        default:
            break;
    }

    return TRUE;
}

void display_help() {
    printf("Usage: video_looper [OPTIONS]\n");
    printf("Options:\n");
    printf("  -v, --video_file       Path to the video file\n");
    printf("  -l, --loopback_device  Path to the loopback device (default: /dev/video61)\n");
    printf("  -a, --audio_sink       Audio sink to use (default: autoaudiosink)\n");
    printf("  -d, --decoder          Decoder to use (hw for hardware, sw for software, default: decodebin)\n");
    printf("  -h, --help             Display this help and exit\n");
}

int main(int argc, char *argv[]) {
    GMainLoop *loop;
    GstBus *bus;
    guint bus_watch_id;
    gchar *pipeline_desc;
    gchar *audio_sink = "autoaudiosink";
    gchar *loopback_device = "/dev/video61";
    gchar *decoder = "decodebin";
    gchar *video_file = "default_video.mp4";

    static struct option long_options[] = {
        {"video_file", required_argument, 0, 'v'},
        {"loopback_device", required_argument, 0, 'l'},
        {"audio_sink", required_argument, 0, 'a'},
        {"decoder", required_argument, 0, 'd'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}
    };

    int option_index = 0;
    int c;

    gst_init(&argc, &argv);

    while ((c = getopt_long(argc, argv, "v:l:a:d:h", long_options, &option_index)) != -1) {
        switch (c) {
            case 'v':
                video_file = optarg;
                break;
            case 'l':
                loopback_device = optarg;
                break;
            case 'a':
                audio_sink = optarg;
                break;
            case 'd':
                if (strcmp(optarg, "hw") == 0) {
                    decoder = "v4l2h264dec";
                } else if (strcmp(optarg, "sw") == 0) {
                    decoder = "avdec_h264";
                }
                break;
            case 'h':
                display_help();
                return 0;
            default:
                display_help();
                return 1;
        }
    }

    loop = g_main_loop_new(NULL, FALSE);

    // Check if loopback device exists
    gboolean loopback_exists = access(loopback_device, F_OK) != -1;

    gchar *video_sink_pipeline = loopback_exists
        ? g_strdup_printf("tee name=t t. ! queue ! autovideosink t. ! queue ! v4l2sink device=%s", loopback_device)
        : g_strdup("autovideosink");

    // Construct the GStreamer pipeline
    pipeline_desc = g_strdup_printf(
        "filesrc location=\"%s\" ! %s ! videoconvert ! %s "
        "filesrc location=\"%s\" ! %s ! audioconvert ! %s",
        video_file, decoder, video_sink_pipeline, video_file, decoder, audio_sink
    );
    g_free(video_sink_pipeline);

    pipeline = gst_parse_launch(pipeline_desc, NULL);
    g_free(pipeline_desc);

    // Set up message handling
    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
    bus_watch_id = gst_bus_add_watch(bus, bus_call, loop);
    gst_object_unref(bus);

    // Start playback
    gst_element_set_state(pipeline, GST_STATE_PLAYING);

    // Run the main loop
    g_main_loop_run(loop);

    // Clean up
    gst_element_set_state(pipeline, GST_STATE_NULL);
    gst_object_unref(GST_OBJECT(pipeline));

    g_source_remove(bus_watch_id);
    g_main_loop_unref(loop);

    return 0;
}
