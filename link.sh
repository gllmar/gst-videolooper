#!/bin/bash

# Check if output source is provided
if [ $# -eq 0 ]; then
    echo "Usage: $0 <output_source_name>"
    exit 1
fi

output_source_name=$1

# Function to connect an output source to an input port
connect_ports() {
    local output_source=$1
    local input_port=$2
    echo "Connecting $output_source to $input_port"
    pw-link "$output_source" "$input_port"
}

# List all input ports
input_ports=$(pw-link -i | grep 'alsa_output')

# Check if the specified output source exists in the output port list
output_exists_FL=$(pw-link -o | grep "${output_source_name}:output_FL")
output_exists_FR=$(pw-link -o | grep "${output_source_name}:output_FR")

if [ -z "$output_exists_FL" ] || [ -z "$output_exists_FR" ]; then
    echo "Specified output source '$output_source_name' does not exist or is incomplete."
    exit 1
fi

# Loop through each input port and connect it to the corresponding output source port
while read -r line; do
    alsa_input=$(echo "$line" | awk '{print $NF}')
    if [[ $alsa_input == *":playback_FL" ]]; then
        connect_ports "$output_source_name:output_FL" "$alsa_input"
    elif [[ $alsa_input == *":playback_FR" ]]; then
        connect_ports "$output_source_name:output_FR" "$alsa_input"
    fi
done <<< "$input_ports"

echo "All matching ALSA inputs have been connected."
