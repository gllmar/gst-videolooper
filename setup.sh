#!/bin/bash

echo "Do you want to install gst-videolooper? [Y/n]"
read -r install_confirmation
install_confirmation=${install_confirmation:-Y}

if [[ ! $install_confirmation =~ ^[Yy]$ ]]; then
    echo "Installation aborted."
    exit 1
fi

# Install dependencies
echo "Installing dependencies..."
sudo apt-get update
sudo apt-get install -y gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly

# Ask if the user wants to install v4l2loopback-conf
echo "Do you want to install v4l2loopback-conf? [Y/n]"
read -r install_v4l2loopback_conf
install_v4l2loopback_conf=${install_v4l2loopback_conf:-Y}

if [[ $install_v4l2loopback_conf =~ ^[Yy]$ ]]; then
    name="v4l2loopback-conf"
    script="setup-$name.sh"
    curl -sSL "https://gitlab.com/gllmar/pi-config/-/raw/main/$name/setup.sh" -o $script
    sudo bash $script
    rm $script
fi



# Ask user for video folder path
echo "Enter the path for the video folder (default: ~/Videos/gst-videolooper):"
read -r video_folder
video_folder=${video_folder:-~/Videos/gst-videolooper}

# Create video folder if it does not exist
mkdir -p "$video_folder"

# Create gst-videolooper service file
echo "Creating gst-videolooper service..."

# Path to your Python script
script_path="$(pwd)/gst-videolooper.py"

# Create the service file
service_file="$HOME/.config/systemd/user/gst-videolooper.service"
mkdir -p "$(dirname "$service_file")"

cat << EOF > "$service_file"
[Unit]
Description=gst-videolooper service
After=wait-for-desktop-loaded.service

[Service]
ExecStart=/usr/bin/python $script_path --loopback-device /dev/video61 $video_folder
Restart=on-failure

[Install]
WantedBy=default.target
EOF

# Ask user to enable the service at boot
echo "Do you want to enable gst-videolooper service at boot? [Y/n]"
read -r enable_boot
enable_boot=${enable_boot:-Y}

if [[ $enable_boot =~ ^[Yy]$ ]]; then
    systemctl --user enable gst-videolooper.service
fi

# Ask user if they want to restart the service now
echo "Do you want to restart the gst-videolooper service now? [Y/n]"
read -r start_service
start_service=${start_service:-Y}

if [[ $start_service =~ ^[Yy]$ ]]; then
    systemctl --user restart gst-videolooper.service
fi

#
# Check if wait-for-desktop-loaded.service exists
wait_for_desktop_service="$HOME/.config/systemd/user/wait-for-desktop-loaded.service"

if [ ! -f "$wait_for_desktop_service" ]; then
    echo "wait-for-desktop-loaded.service does not exist. Do you want to create it? [Y/n]"
    read -r create_service
    create_service=${create_service:-Y}

    if [[ $create_service =~ ^[Yy]$ ]]; then
        # Create wait-for-desktop-loaded.service file
        cat << EOF > "$wait_for_desktop_service"
[Unit]
Description=Wait for wf-panel to finish loading
After=graphical-session.target

[Service]
Type=oneshot
ExecStart=/bin/true
ExecStartPost=/bin/bash -c "while ! pgrep -x 'wf-panel-pi' > /dev/null; do sleep 4; done"

[Install]
WantedBy=default.target
EOF

        # Enable the service
        systemctl --user enable wait-for-desktop-loaded.service
    fi
fi

echo "Installation completed. Please ensure you add video files to $video_folder."
